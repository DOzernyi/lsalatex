---
layout: default
---

# Compiled examples are [here](https://www.overleaf.com/read/pncdfwhgdqzx).

* * *

# Glossing and examples in <code>expex</code>

## <code>expex</code> for examples

<code>expex</code> is, it seems, the most powerful and the most flexible tool for glossing out there. Glossing isn't the only thing <code>expex</code> does, of course. However, first:

> You can read the documentation [here](https://ctan.org/pkg/expex?lang=en). It's long but it's worth it. This page only provides examples for rudimentary usage.

The most basic use is to introduce a numbered example:

> Disclaimer: all the examples below require \usepackage{expex} in the preamble (trivially)!

```TeX
\pex % Optional label
Some text.
\xe
```

Equivalently:

```TeX
\ex % Optional label
Some text.
\xe
```

Examples that have multiple elements (or anything numbered, actually -- not only examples), can be formatted as:

```TeX
\pex % Optional label
\a Some text which will be labeled ``a.''.
\a Some text which will be labeled ``b.''.
\a Some text which will be labeled ``c.''.
\xe
```

For setup which would yield multiple columns, see instructions on the [trees page](./docs/pages/trees.md); you'll use multicol (you _can_ use multicols, but there are both <code>expex</code>-internal ways to get this layout, and tools other than multicols). But you can absolutely look at documentation for <code>multicol</code>, [the documentation](https://ctan.org/pkg/multicol?lang=en) is fairly short. (Also, see below for <code>glwidth</code> and its use.)

## Glosses

Glosses can be difficult to manage. The generic setup is as follows (example from _PDA_'s template but without bits that might require IPA symbols from <code>tipa</code>).

```tex
%the settings on the line below are to make sure that the gloss conforms to LSA's stylesheet;
%LSA requires (a) no italics in the gloss, (b) no additional space between the last translation line
\pex[everygla={},aboveglftskip=0em]
%the line below begins the gloss environment itself
\begingl
%in ``gla'' below, gl is for gloss; _a_ is for one of the levels _a,b,c_
%level a is the original sentence in whatever language it is
\gla \~nuka-ka Maria papa-ta yanu-chi-shka ka-rka-ni.//
%level b is the gloss itself;
%line wrapping and alignment with the sentence in \gla is automatic
\glb 1{\sc sg}-{\sc top} Maria potato-{\sc acc} cook-{\sc caus}-{\sc pass} be-{\sc pst}-{\sc 1sg.sbj}//
%lastly, \the line below is the free translation
\glft `I was made to cook potatoes by Maria.//
% note the quotation marks: ` and ' or `` and ''
%close the environment
\endgl
\xe
```
Note the use of
```tex
{\sc text}
```
to get small caps. Be careful not to use just <code>\sc</code> as opposed to <code>{\sc}</code> which will result in the entirety of what follows being rendered in small caps. _Always_ in LaTeX, if you open something (say, an environment), you need to close it somewhere -- or it will only close with <code>\end{document}</code> (that's the best possible case; in other cases your document just won't compile).

Note also that there should be one and only one <code>\gla</code>, whereas <code>\glb</code> and <code>\glc</code> can be many. See below for why you might need this (example from expex documentation, p. 39):

```tex
\ex[everygla={},aboveglftskip=0em]
\begingl
\gla k- wapm -a -s’i -m -wapunin -uk //
\glb CL V AGR NEG AGR TNS AGR //
\glc 2 see {\sc 3acc} {} {\sc 2pl} preterit {\sc 3pl} //
% *Note* that in all levels {a,b,c}, there are 7 elements! This is important
% to format the gloss appropriately, this also follows Leipsiz rules.
\glft `you (pl) didn’t see them'//
\endgl
\xe
```

Speaking of Leipzig rules, they are useful to know; see [here](https://www.eva.mpg.de/lingua/resources/glossing-rules.php).

It is sometimes useful to cascade gloss so that it is split into multiple lines (e.g., when it's a long sentence and clauses are better displayed on separate lines). This is achieved as follows (example from expex documentation, p. 40):

```tex
\ex[everygla={},aboveglftskip=0em]
\begingl
%note the use of in-line math $_i$ for indexing
%the + sign *inserted !only! in \gla* and not anywhere else cascades the lines
\gla Mary$_i$ ist sicher,
% here the lines starting with + are split each for new line in Tex code, this isn't necessary
+ dass es den Hans nicht st\"oren w\"urde
+ seiner Freundin ihr$_i$ Herz auszusch\"utten.//
\glb Mary is sure that it the-{\sc acc} Hans not annoy would
his-{\sc dat} girlfriend-{\sc dat} her-{\sc acc} heart-{\sc acc} {out to
throw}//
\glft `Mary is sure that it would not annoy John to reveal her
heart to his girlfriend.'//
\endgl
\xe
```

An alternative way of splitting the gloss in two columns is <code>glwidth</code>. See below (example from expex documentation, p.47).

```tex
% the key here is the parameter [glwidth=NUMin]
% the exact NUM will depend on the size of the paper you're using, etc.
\ex[everygla={},aboveglftskip=0em,glwidth=2.1in]
% ``a.'' is typical for expex, but note \quad
a.\quad
\begingl
\gla Mary$_i$ ist sicher, dass es den Hans nicht st\"oren w\"urde
seiner Freundin ihr$_i$ Herz auszusch\"utten.//
\glb Mary is sure that it the-{\sc acc} Hans not annoy would
his-{\sc dat} girlfriend-{\sc dat} her-{\sc acc} heart-{\sc acc} {out to
throw}//
\glft `Mary is sure that it would not annoy John to reveal her
heart to his girlfriend.'//
\endgl
% note ``b'' is used that is not as typical for expex
b.\quad
\begingl
\gla Mary$_i$ ist sicher, dass seiner Freunden ihr$_i$ Herz
auszuch\"utten dem Hans nicht schaden w\"urde.//
\glb Mary is sure that his-{\sc dat} girlfriend-{\sc dat} her-{\sc acc}
heart-{\sc acc} {out to throw} the-{\sc dat} Hans not damage would//
\glft `Mary is sure that to reveal her heart to his girlfriend
would not damage John.'//
\endgl
\xe
```

> For those interested, \quad appears to be from Italian quadratone. In LaTeX, it's associated with width.

The last thing discussed here will be a very useful option of side panel in glosses. That is, you can have a gloss on the left, and some comment about the gloss on the right in a two-column setup. Below is a (long, but rewarding) example of this from expex documentation, page 54. There are some significant changes to content, and minor changes to code. This setup does not require any complex additions to what has been discussed heretofore, but can come very handy.

```tex
% \footenotizesize fixes the font size for comment panel since the usual font size might be too big
\ex[everypanel=\footnotesize,everygla={},aboveglftskip=0em]<panelex>
% this is for small caps, can be omitted
\let\\=\textsc
% this the important bit that begins the first panel (aka first column);
% nevermind ssratio and glhangstyle, just take them as they are
\beginglpanel[ssratio=.5,glhangstyle=none]
% here's your gloss
\gla Mary$_i$ ist sicher, dass es den Hans nicht st\"oren w\"urde seiner Freundin ihr$_i$ Herz auszusch\"utten.//
\glb Mary is sure that his-{\sc dat} girlfriend-{\sc dat} her-{\sc acc}
heart-{\sc acc} {out to throw} the-{\sc dat} Hans not damage would.//
\endgl
% after \endgl, start the right panel, ie the space where you write whatever comment you wanted
1. This is the first note. \par
2. This is the second note. \par
3. This is the third note, which is longer than other notes to show that notes can be longer than one line and will still maintain the correct layout. \par
4. This is the fourth note. \par
\endpanel
% This is the translation that follows below the entire gloss-comment setup.
\bigskip
`Mary is sure that to reveal her heart to his girlfriend would not damage John.'
\xe
```

> The original code contained <code>\enspace</code> after every number in the comment section. I am not sure it's really necessary. See [this useful thread](https://tex.stackexchange.com/questions/41476/lengths-and-when-to-use-them) on spacing and commands like <code>\enspace</code> and <code>\enskip</code> on stackexchange.

> For those interested, <code>ssratio</code> is for the proportion of the width (of the page minus margins) that the gloss occupies; so 0.6 would have given 60% of the width to the gloss and the remaining 40 to the panel with the comment. <code>glhangstyle</code> is for indentation of interlinear gloss, comes in three kinds: none, normal, and cascade.

> Crucially, please do note that just copying examples from the documentation is usually not helpful. If you're lucky, they'll work. Often, however, they require additional packages, or can't be compiled at all in the environment or with engine you're trying to use them in. For example, this could be the case if you were to just copy the panel example from p. 54 of expex documentation. So *read* the relevant bit of the documentation first.

This should pretty much cover the basic uses of glossing; for more advanced manipulations consult the documentation [here](https://ctan.math.illinois.edu/macros/generic/expex/expex-doc.pdf). One item which I think is particularly useful is the labeling tool, to the effect of LaTeX's default <code>\label{some_label}</code> which is not visible in the object (.pdf output, that is), but can be called (almost) anywhere in the text as <code>\ref{some_label}</code> and output the number which expex assigned and create hyperref to the page on which the referenced object appears. Expex expands on this and has its own extensive system of referencing. This can be quite useful when writing longer papers.

* * *

# Compiled examples are [here](https://www.overleaf.com/read/pncdfwhgdqzx).

[back](https://dozernyi.gitlab.io/lsalatex)
