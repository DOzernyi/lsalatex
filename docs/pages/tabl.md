---
layout: default
---

# Compiled examples are [here](https://www.overleaf.com/read/nsvwmfnjzsyg).

* * *

## Tableaux are just tables

Despite having a fancy francophone ring to them, tableaux are just regular tables for the purposes of LaTeX. They are mere tables that appear in a particular place and have particular formatting -- which actually simplifies the task of typesetting them in a way.   

## Table and tabular

### Simple tables

So, usually, when you type in <code>\begin[some_environment]</code>, LaTeX (most editors, but we focus on Overleaf in particular) automatically closes it with <code>\end[some_environment]</code> so that you don't forget to close the environment. At the same time, when you begin figure or table, you get more help. What happens with figures shall remain a mystery since the page is about tables, but if you enter <code>\begin{table}</code> and are lucky, you get the following bit of code:

```tex
\begin{table}[]
    \centering
    \begin{tabular}{c|c}
         &  \\
         &
    \end{tabular}
    \caption{Caption}
    \label{tab:my_label}
\end{table}
```

This has a couple of elements which are worth going over (even though Overleaf or any intro to LaTeX would do as well). Begin-table essentially creates a table proper (which is the tabular environment), but also centers it (<code>\centering</code>), adds caption to the table, and labels it (labels are not shown, but can be called in text with <code>\ref{...}</code> to create a hyperlink to the table within the text). For tableaux, there is not need for centering or for a caption of the kind above. So, a simple example of a table _within the expex environment_ (<code>\pex...\xe</code>, that is) will create a numbered example which consists of a table (expex can hold almost anything, which is admirable).

So, here's a more fleshed out example of a simple table:

```tex
Some text before the table.

% by default the line above will not be indented if it follows a section title
\begin{table}[h!]
% the h! bit tells to place the table right where it is mentioned in text; not where it fits on the page (LaTeX can be lenient with tables, letting them float for better effect)
    \centering
% centering, trivially
    \begin{tabular}{c|c}
% the {c|c} bit tells that there are two columns, to center, and to put a vertical line between two columns
        LSLT & weird algebraic notation \\
% "\\" is *very* important
        MP & legerdemain with set theory
% true facts about the MP
    \end{tabular}
    \caption{Caption}
    \label{tab:my_label}
\end{table}

Some text following the table.
```

### Friendship of tabular and expex

Here's a simple example of a table as a numbered example with expex:

```tex
\pex
    \begin{tabular}{c|c}
    Why didn't they ask Evans? & Hickory dickory dock \\
    By the pricking of my thumbs & A pocket full of rye
   \end{tabular}
\xe
```

And one with two tables a and b for one numbered example, as per usual for expex:

```tex
\pex
\a     \begin{tabular}{c|c}
        Titus Andronicus & Tamora \\
        Richard III & George, Duke of Clarence
       \end{tabular}
\a     \begin{tabular}{c|c}
        William Blake & Endless night \\
        Alfred Tennyson & Lady of Shalott
       \end{tabular}
\xe
```

Note that for the tables above, you _only_ need expex (or nothing, for the very first table). Now, here's a more involved example of a table with some comments on styling options:

```tex
\pex
\a  Things ordinary people can't play.  \\
        \begin{tabular}{l|c|l}
% note l instead of c above creating left alignment
        \hline
% hline creates a horizontal line
        \textcolor{red}{Schumann} & \textit{Davidsbundlertanze} & too many notes \\
        \hline
        Shostakovich & \textbf{Prelude and fugue in D-flat major, Op. 87} & even more notes
       \end{tabular}
% vspace inserts space between tables; but this is advisable only if this can't be specified with expex,
% or if it's a one-time thing whereas other examples do not require such spacing       
       \vspace{1em}
\a  Some caption \\
% this uses package array for what follows begin-tabular and fixes width of the columns
       \begin{tabular}{||m{3cm}|m{4cm}|m{1cm}||}
       \hline
        James I of Scotland & Daemonologie & 1597 \\
        \hline
        \hline
        Kramer and Sprener & Malleus Maleficarum & 1496 \\
        \hline
       \end{tabular}
\xe
```

## Tabularray

For more complex tables -- like the one from the _PDA_ template -- you will need package <code>tabularray</code> (or so is recommended; feel free to achieve the same ends with whatever means, even if it's TikZ). Here's the table from _PDA_ with some comments:

```tex
\pex /m\'{u}-bet\textipa{\`E}/ $\rightarrow$ [m\'ibet\textipa{\`E}̀] `mouth'\\
% the bit above requires tipa and \UseRawInputEncoding
% also note the you are beginning not tabular, but tblr
    \begin{tblr}[]
% colspec is where you, well, spec(ify) col(or)
% but also give the settings for lines and alignment
    {colspec = {l c|c|c|[dashed]r},
% the line below says that fifth cell in second row should have color grey9
    cell{2}{5} = {gray9},
% the colors available are 81, and they come from package ninecolors
% which -- bonus -- is automatically loaded by tabularray
    cell{3}{5} = {gray9},
    cell{4}{5} = {gray9},
    cell{5}{5} = {gray9},
    cell{3}{4} = {gray9},
    cell{4}{4} = {gray9}
% close the colspec environment
    }
% this is where the actual table starts
     \hline
     & m\'u-bet\textipa{\`E} & {\sc faith}($\alpha${bk}) & {\sc (*a-span)}($\alpha${bk}) & {\sc fgdsp}($\alpha${bk})\\
      \hline
     a. & (m\'u)(bet\textipa{\`E}) & & *! & *\\
     b. & (m\'u)(bat\`{a}) & !* & * & **\\
     c. & (m\'u)(bat\`{a}) & *!* & & **\\ \hline[dashed]
% note how you can have a dashed hline
     d. \PointingHand & (m\'i)(bet\textipa{\`E}̀) & & & *\\
% weird symbols like PointingHanf require packages, this one in particular is from marvosymb package
     \hline
    \end{tblr}
\xe
```

The beauty of tables is that you can indeed -- provided you loaded the requisite packages -- copy the example and use it to suit your needs. Some further notes:

* here's [documentation for tabularray on CTAN](https://ctan.org/pkg/tabularray?lang=en); it's quite flexible and its use extends beyond what is shown above
* as noted above, <code>tabularray</code> _automatically_ loads package <code>ninecolors</code> (which has 81 colors), the CTAN documentation for which is [here](https://ctan.org/pkg/ninecolors?lang=en); find the list of available colors on p. 1 (out of 4!)

The symbols (such as <code>\PointingHand</code>) will be briefly discussed on [the page devoted to symbols](https://dozernyi.gitlab.io/lsalatex/docs/pages/symbols.html).  

#### Please also note that there is a web tool for creating the tables (pointed out to me by Hossep Dolatian) which can be found [here](https://meluhha.com/tableau/). Yet, in the didactical interest, I would advise to create the tableaux on one's own is the best course of action. 

* * *

# Compiled examples are [here](https://www.overleaf.com/read/nsvwmfnjzsyg).

[back](https://dozernyi.gitlab.io/lsalatex)
