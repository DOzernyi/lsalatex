---
layout: default
---

> This is to reiterate that none of the suggestions below are binding for submissions to PLSA/PDA/EML, &c.. It is not impossible to accommodate the requirements of the journals in the systems other than the one described below.  

## IPA and <code>tipa</code>

IPA, incidentally, can stand for at least three things: International Phonetic Alphabet, (famously) India pale ale, and (not so famously) [a species of spider](https://species.wikimedia.org/wiki/Ipa). It is the phonetic bit that will concern us here. IPA, really, is just a set of symbols that constitute a very particular kind of headache for typesetting. IPA is used by fewer people than math mode (so it received less attention), but the gist is essentially the same: you have some special symbols which you call with certain commands in a certain environment. Now, this is one way to go about IPA. This is the view where IPA requires some particular environment. There's a different way of thinking, where IPA symbols are just symbols -- as any other symbols --, and they can be copied/inserted right within the text without any special treatment (like calling an environment).

Both of these ways of thinking about IPA have implementations in TeX (there are more, but we'll focus on these two; they are also not entirely mutually exclusive, though don't use both please unless you're absolutely sure what you're doing). The first way is conventionally achieved by using <code>tipa</code> package. It is (pretty much) the only package which allows for this kind of manipulations (IPA in a special environment; there were packages that pre-dated tipa, but they are essentially old version of tipa). The second way is usually achieved by using package called <code>fontspec</code>. What fontspec allows is to load OpenType fonts in a TeX document. It allows for a lot of flexibility while using fonts. So, (essentially) IPA symbols can indeed be inserted directly in your document. It is worth noting that fontspec can be loaded _only_ in XeLaTeX and LuaLaTeX -- _not_ in LaTeX or pdfLaTeX. (More on this in the paragraph below.) The first way is the way which IPA template for _PDA_ takes (and _PLSA_ too). There are reasons for this, and they are also briefly summarized below.

> The paragraph below is a skimpy attempt to briefly summarize what evolved over 40 years of work on TeX and its derivatives. If you're interested (and I hope you are), read more [here (links to wiki)](https://en.wikipedia.org/wiki/TeX), [here (links to Overleaf)](https://www.overleaf.com/learn/latex/Articles/What%27s_in_a_Name%3A_A_Guide_to_the_Many_Flavours_of_TeX), or [here (links to a YouTube clip with Donald Knuth who developed TeX)](https://www.youtube.com/watch?v=9IM_dqjBkIw).

Regarding LaTeX vs. XeTeX vs XeLaTeX vs LuaLaTeX vs pdfLaTeX, etc. Essentially, the typesetting system TeX was frozen by the gentleman who invented it, viz. Donald Knuth, in the 1980s. He did not want to add any more features, and so he didn't. Ever since, he's been periodically fixing bugs in the system. Yet, people wanted to extend TeX -- which was and is freely available -- and that's how, e.g. LaTeX appeared. (It couldn't be called TeX because Knuth wanted to keep derivatives separate from his own work -- and was right to do so.) LaTeX was (and is) a set of macros (functions or patterns, broadly construed) for TeX. At the same time, it seemed like outputting TeX input as a .pdf is a good idea. That's how _engin_ pdfTeX appeared (extend that to pdfLaTeX). XeTeX is also an engin that is particularly useful for complex font systems like Arabic alphabet, or other writing systems on which less work has been done (for whatever reasons). In pdfLaTeX, <code>babel</code> is usually the package that loads alphabets other than roman ones -- but it gets rather messy rather soon. XeLaTeX (and its own version of babel -- <code>polyglossia</code> manage better). LuaTeX (and so LuaLaTeX) is a more recent development, and is probably the most flexible one. It is also an engin.

So, the initial template for _PDA_ (before a massive revision in June 2022) used a mix of tipa and fontenc (while not compiling them properly anyway), which I do not find advisable generally and for templates in particular. It requires familiarity with both system and careful use of them. E.g., when someone is switching from tipa to fontenc they might use packages such as <code>xunicode</code> or <code>tipauni</code> which convert tipa symbols to UTF (Unicode Transformation Format). Note that, e.g., xunicode runs only on LuaLaTeX. For a _template_, I thought and still do think that such a setup is undesirable when all that is needed can be achieved just with tipa. Perhaps some flexibility is sacrificed (and readability of the code, which tipa is sort of notorious for; you'll see why below) -- but in the spirit of on the other hand coherency, on the other hand accessibility -- tipa is the default choice for the LSA templates. It is by no means mandated, however. Anything goes as long as the output follows the guidelines.

Another rather technical reason for choosing tipa was the fact that fontspec's Times New Roman (as Times New Roman generally, there's one really) does not natively support small caps -- which are requisite for glosses; and Times is the required font for _PDA_. I tried various fake caps, but none of the ones I could easily come up with achieved the purpose. Further, controlling the kind of font which appears for fontspec-type inserted symbols can be complex to grasp. In tipa's case, however, various fonts are available, and with an old <code>times</code> package, consistent fonts can be achieved without user needing to worry about it. Also, tipa generally has great flexibility with fonts (the xipa classes, e.g., xipa10 and so on) -- but not all of them are friends with Overleaf yet.

Now, to the use of tipa. As mentioned above, it's quite straightforward, really (that is, for our purposes here; tipa is rather more tangled than it seems one you go beyond basic usage).

> As per usual, please do consult [documentation on CTAN](https://ctan.org/pkg/tipa?lang=en).

The package takes some getting used to. For example, the following code is IPA's version of the word _explanation_:

```tex
% from p. 13 of tipa documentation
\textipa{[""Ekspl@"neIS@n]}
```

The package is also flexible in another dimension; the same output (_explanation_) can be achieved with the following input:

```tex
% ibid.
[\textsecstress\textepsilon kspl\textschwa
\textprimstress ne\textsci\textesh\textschwa n]
```

The "environment" mentioned above (in analogy to <code>$...$</code> for in-line math or <code>\[...\]</code> for equations) can be called in different ways (one of them already appeared in the first example):

```tex
% ibid
\textipa{...}
% or
{\tipaencoding ...}
% or
\begin{IPA} ... \end{IPA}
```

The documentation also provides useful advice on achieving better effect for alternative ways of calling the same symbol. For example, the author suggests that

```tex
% p. 14 of documentation
[\textipa{S}]
```

is worse than

```tex
% ibid
\textipa{[S]}
```

I am not entirely sure why one of them is better than the other. Lastly, there is no reason to list a number of phonetic symbols on this page -- they are all available in package documentation. Further, since needs vastly differ depending on the subfield of phonetics/phonology, the relevant symbols will differ respectively. Most of them are given either in the first 20 pages of tipa documentation, or in the appendices.

> Note that there is no Overleaf page with the examples above -- please consult package documentation for all of them (there are just five of them on this page, and all of them come from pages 13-14).

[back](https://dozernyi.gitlab.io/lsalatex)
