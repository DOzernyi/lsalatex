---
layout: default
---

## Instructions for compiling on Overleaf

1. Go to the page for the requisite LSA template on Overleaf (choose from the links in the header on the left).
1. Choose _Open as Template_.
1. If Overleaf prompts you to log in (if you don't have an account), register or log -- whichever applies.
1. Write your paper and make sure you have a proper .bib file.
1. Click _compile_ in the top menu.
1. Fix errors if need be; and make sure the bibliography as well as the rest of the paper compile properly.
1. Download .pdf using either the main menu in top left corner, or the .pdf download option next to compile button.
1. Submit your .pdf to _PLSA_/_ELM_, etc. website.  

> Note that if you are not familiar with TeX, compiling and code generally, it might be instructive to look through through the examples of code which are available on the home page.

## Compiling locally and why we won't give instructions for that

Given the target audience of this website, compiling locally is not really a good option (perhaps even really not a good option) unless you already have the requisite software installed and use LaTeX frequently. If the latter is the case, then there's nothing new to be told here. Overleaf is indeed strongly suggested for beginners; whether because TeXLive _can_ take 12 hours -- if you don't know how to pre-select packages -- to install (better with MiKTeX, but it's dealer's choice with some security trade-offs), or because there's no space here to go over different text editors, each of which requires individual approach. _Atom_, where this website was written ~~and which I use,~~ (RIP _Atom_ as of December 2022) might not fit someone, and packages for _Atom_ trivially are of no use for _Vim_, etc. So, the recommendation for the very beginner is to start with Overleaf which is very straightforward.

The templates which are in the repo were compiled in _Atom_ (mainly) with _Atom_LaTeX_ package. The templates are not quite perfect, this is especially true for the .bib files, where a lot of tweaking took place. It's possible that .bst will be updated at some point in the future. Pretty much in every case, .bib that was not created by hand (as it shouldn't be), but is output of some bibliography management system (e.g., Zotero), will need to be cleaned. This (the .bib part of it) is one of the reasons log files are included in the repo in each folder (_PLSA_ and _PDA_). Logs contain more information about what you might want to alter as you use the templates, e.g.:

```
// from PDA_main.log, 725-725
LaTeX Font Info: Font shape 'OT1/phv/m/it' in size <9> not available
(Font)           Font shape 'OT1/phv/m/sl' tried instead on input line 206.
```

or

```
// from PLSA_template.blg, 46
warning$ -- 2
```

To the same effect, all the overfulls and underfulls are listed if someone really wants to fix them:

```
// from PLSA_template.log, 590-593
Underfull \hbox (badness 10000) in paragraph at lines 90--90
          [][][]\OT1/ptm/m/n/10 Both ac-knowl-edg-ments and au-thor af-fil-i-a-tion in-f
          or-ma-tion go in an ini-tial foot-note like...
```

Naturally, when you write your paper, there exact errors are unlikely to be applicable.

> **Note** that in the GitLab repo, there is a file  **PDA_preamble.tex** that happens to be empty, yet still a part of the template. In the Overleaf template, it isn't empty. However, some local compilers have personal issues with <code>\include[path]</code>, so what was in the preamble on Overleaf is in root file locally. The empty preamble file was left just not to create an impression a file is missing -- and to use for those whose local compilers aren't misbehaved. Mischief is, of course, correctable in every case, but it might take extra steps which can otherwise be avoided as the emphasis for this intro guide is on parsimony. Further, the way to fix this will depend on the editor, engin, etc.

> GitLab repo also contains .zip-s of the Overleaf sources, should that be of interest.

[back](https://dozernyi.gitlab.io/lsalatex)
