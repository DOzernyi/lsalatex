---
layout: default
---

# Compiled examples are [here](https://www.overleaf.com/read/zzpsdkpsyrcc).

* * *

## Symbols

There are many symbols that can be called with LaTeX. In fact, [this](http://tug.ctan.org/info/symbols/comprehensive/symbols-a4.pdf) document -- _The Comprehensive LaTeX Symbol List_ (last updated 5 May 2021) -- lists 18 150 symbols you can use. These come _in_ packages, _with_ packages, etc. This should cover more than you can think of, and likely more than you have seen in your life. There's no point in just listing them here, so just look through the document at your leisure. For those of us in phonetics, page 189 contains the description of package <code>marvosym</code>, which has pointing hands, oft used in "tableaux".

## Math and logic

It is perhaps useful to give a couple of examples of symbols used in math that is related to linguistics. The examples below were taken mostly from linguistic literature, so are likely to be faced by a student. The text below contains fragments of code and source without almost any comments -- consult the link on the homepage to see the compiled version.

## Examples

### Matrices in sentences

> The following example is from "Arc Pair Grammar" by Johnson and Postal, Princeton UP 190, p. 330, ex. 91, Def. 143

```pex
\pex Let $Q$ be a constituent of $X$ whose point is node $a$, where $A \in Q$. Then:\\
\[
% very different from \begin{bmatrix}! (that one is used on the page for LFG-style structures)
\begin{Bmatrix}
\text{Persistent}(A, Q)\\
\text{Persistent}(A, a)
\end{Bmatrix}
\longleftrightarrow \text{Not}((
\exists B)(\text{Erase}(B,A) \vee B \in Q))\]
\xe
```

### (Somewhat) Complex math formulae

> The following example is from "The Mathematics of Language" by Marcus Kracht, Mouton de Gruyter 2003; p. 182 ex. 3.8.

```pex
Let $\mathfrak{Tm}_\Omega := \langle\text{PN}_\Omega,\{g^{\mathfrak{Tm}_\Omega}: g \in F\}$, where $\text{PN}_{\Omega}$ is the set of constant $\Omega-$terms written in Polish notation and
\pex[exno=3.8,numoffset=2em]
$g^{\mathfrak{Tm}_\Omega}(\vec{x_0},...,\vec{x}_{\Omega(g)-1}) := g^\frown \prod_{i<\Omega(g)} \vec{x}_i$
\xe
$\mathfrak{Tm}_\Omega$ is a freely o-generated $\Omega$-algebra.
```

### Logic/semantics

> The following example is from "And: Conjunction Reduction Redux" by Barry Schein, MIT Press 2017. Section on number agreement in the DP chapter, p. 142, ex. 329

```tex
\pex[exno=329,numoffset=2em] $\exists e \text{Laughed}(e) \wedge$
\\ $[\text{every } x : \text{woman}(x)][\exists e' : e' \leq e] ... W_i(e',x) \wedge$
\\ $[\rotatebox[origin=c]{180}{$\iota$}e': pro_i][\forall e: e \leq e']$
\\ $[\rotatebox[origin=c]{180}{$\iota$}E'' : C[e',E''] \wedge \exists Y W[E'', Y]]$
\\ at $[E'', \text{a linguistics professor}]] e \leq E''$,
\\ and
\\ $[\forall e': e' \leq e \wedge [\exists x: \text{woman}(x)] ... W(e',x)...][\exists e'' : C(e',e'')]$
\\ $[\rotatebox[origin=c]{180}{$\iota$}x: \text{her child}(x)]_j ... W(e'',x) ... \wedge$
\\ $[\rotatebox[origin=c]{180}{$\iota$}E'': C[e', E''] \wedge \exists Y W[E'', Y]]$
\\ at $[E'', \text{a linguistics professor}]))$
\xe
```

### Modal

> The following example is from "Set Theory and the Continuum" by Raymond M. Smullyan and Melvin Fitting, Oxford 1996. It's a couple of formulas from pages 198-199 (intro to forcing). They require <code>amsfonts</code>.

```tex
\pex
\a $\Box\Diamond(\Box X \supset \Box\Diamond Y)$
\a $\llbracket X \rrbracket \equiv \Box\llbracket X \rrbracket$
\a $(\exists x)\llbracket\phi(x)\rrbracket \supset \llbracket (\exists x)\phi(x) \rrbracket$
\a $(\Box\Diamond X \wedge \Box\Diamond Y) \equiv \Box\Diamond(\Box\Diamond X \wedge \Box\Diamond Y)$
\xe
```

### Lattices

> The following is an example of a free Heyting algebra. It is spelled in every possible detail, which is quite laborious. Upon closer examination, the code can be reduced to half (or less) of the fragment below.

```tex
\begin{tikzpicture}[x=1.5cm,y=1.5cm]% <-- change this numbers if you need (separations)
% nodes
\node at (0,0)    (o)  {$\bot$};

\node at (-1,1)   (a1) {$\neg\phi$};
\node at (1,1)    (a2) {$\phi$};
\node at (0,2)    (b1) {$\phi \vee \neg\phi$};
\node at (2,2)    (b2) {$\neg\neg\phi$};
\draw (o) -- (a1);
\draw (o) -- (a2);
\draw (a2) -- (b2);
\draw (a1) -- (b1);
\draw (b1) -- (a2);

\node at (-1,3)   (c1) {$\neg\neg\phi\rightarrow\phi$};
\node at (1,3)    (c2) {$\neg\phi\vee\neg\neg\phi$};
\node at (0,4)    (d1) {$\neg\neg\phi\vee(\neg\neg\phi\rightarrow\phi)$};
\node at (2,4)    (d2) {};
\node at (2.5,4) {$(\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi)$};
\draw (b1) -- (c1);
\draw (b2) -- (c2);
\draw (b1) -- (c2);
\draw (c1) -- (d1);
\draw (c2) -- (d2);
\draw (d1) -- (c2);

\node at (-1,5)   (e1) {};
\node at (-2.5,5) {$((\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi))\rightarrow(\neg\phi\vee\neg\neg\phi)$};
\node at (1,5)    (e2) {};
\node at (2.5,5) {$(\neg\neg\phi\rightarrow\phi)\vee((\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi)$};
\node at (0,6)    (f1) {};
\node at (-2.6,6) {\shortstack{$(((\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi))\rightarrow(\neg\phi\vee\neg\neg\phi))$\\$\vee((\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi))$}};
\node at (2,6)    (f2) {};
\node at (3.75,6) {\shortstack{
$(((\neg\neg\phi\rightarrow\phi)\rightarrow(\phi\vee\neg\phi))\rightarrow$ \\
$(\neg\phi\vee\neg\neg\phi))\rightarrow(\neg\neg\phi\vee(\neg\neg\phi\rightarrow\phi))$}};
\draw (d1) -- (e1);
\draw (d2) -- (e2);
\draw (d1) -- (e2);
\draw (e1) -- (f1);
\draw (e2) -- (f2);
\draw (f1) -- (e2);


\node at (-1,7)   (g1) {...};
\node at (1,7)    (g2) {...};
\node at (0,8)    (h1) {...};
\node at (2,8)    (h2) {...};
\draw (f1) -- (g1);
\draw (f2) -- (g2);
\draw (f1) -- (g2);
\draw (g1) -- (h1);
\draw (g2) -- (h2);
\draw (h1) -- (g2);

\node at (-1,9)   (q1) {...};
\node at (1,9)    (q2) {...};
\draw (h2) -- (q2);
\draw (h1) -- (q2);
\draw (h1) -- (q1);
```

* * *

# Compiled examples are [here](https://www.overleaf.com/read/zzpsdkpsyrcc).

[back](https://dozernyi.gitlab.io/lsalatex)
