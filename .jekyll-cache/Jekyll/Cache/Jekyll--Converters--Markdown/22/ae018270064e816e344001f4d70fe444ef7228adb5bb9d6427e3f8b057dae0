I"�<h2 id="about">About</h2>

<p>This website should provide a more or less comprehensive guide to LaTeX which is used in the LSA templates, namely:</p>

<ul>
  <li><em>Proceedings of the Linguistic Society of America</em> (template by Sarah E. Murray, Patrick Farrell, Daniil M. Ozernyi)</li>
  <li><em>Phonological Data and Analysis </em> (template by Daniil M. Ozernyi, Patrick Farrell, Laura R. Faircloth)</li>
  <li>we also link the template for <em>Experiments in Linguistic Meaning</em> (template by Florian Schwarz), but please see <em>ELM</em>’s <a href="https://journals.linguisticsociety.org/proceedings/index.php/ELM/pages/view/instructions">website</a> for instructions.</li>
</ul>

<p>While the templates do contain examples of bare essentials, they do not introduce readers to TeX and are primarily aimed at those already familiar with it. So, this website will hopefully cover the basics of most packages needed for classroom use (be it a syntax, semantics, phonology, or some other class requiring TeX) or for LSA templates. Note that TikZ is not discussed at length (only in conjunction with expex), and neither is BibLaTeX. The stress is on <a href="https://www.overleaf.com/project">Overleaf (links to an external site)</a>, which is (pretty much without doubt) easier for beginners both to grasp and to use.</p>

<p>My hope is that the information given here and the ease of accessing <a href="https://www.overleaf.com/project">Overleaf</a> will encourage more people to use (La)TeX in their work.</p>

<p><strong>Crucially</strong>, however, the usage of the templates given here is <strong>by no means</strong> restrictive: provided that the final submission fits the requirements given by PLSA/PDA/ELM, any means to get that result is perfectly fine.</p>

<blockquote>
  <p>Before you look at the pages below and the examples of implementation of the content covered here, please do consult a (very) concise Overleaf 30-minute introduction to LaTeX <a href="https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes">here</a>.</p>
</blockquote>

<table>
  <thead>
    <tr>
      <th style="text-align: left"> </th>
      <th style="text-align: left">Page with brief intro and code with comments</th>
      <th style="text-align: left">Overleaf link with clean code and compiled examples</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left">1</td>
      <td style="text-align: left"><a href="./docs/pages/compiling.html">Compiling (Overleaf, local)</a></td>
      <td style="text-align: left">–</td>
    </tr>
    <tr>
      <td style="text-align: left">2</td>
      <td style="text-align: left"><a href="./docs/pages/trees.html">Trees (qtree, forest, multidominance, lambda)</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/pwvbtzfknmcj">link</a></td>
    </tr>
    <tr>
      <td style="text-align: left">3</td>
      <td style="text-align: left"><a href="./docs/pages/gloss.html">Glosses and examples (expex)</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/pncdfwhgdqzx">link</a></td>
    </tr>
    <tr>
      <td style="text-align: left">4</td>
      <td style="text-align: left"><a href="./docs/pages/ipa.html">IPA (tipa, some notes on XeLaTeX/LuaLaTeX)</a></td>
      <td style="text-align: left">–</td>
    </tr>
    <tr>
      <td style="text-align: left">5</td>
      <td style="text-align: left">References (natbib, .bst) [TBD]</td>
      <td style="text-align: left">[TBD]</td>
    </tr>
    <tr>
      <td style="text-align: left">6</td>
      <td style="text-align: left"><a href="./docs/pages/tabl.html">Tableaux (just tables, really; tabularray and tabular)</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/nsvwmfnjzsyg">link</a></td>
    </tr>
    <tr>
      <td style="text-align: left">7</td>
      <td style="text-align: left"><a href="./docs/pages/pro.html">PRO- and trace-arrows for linear examples</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/vqysvzvqjkxx">link to 7</a>, <a href="https://www.overleaf.com/read/srvqkqcwwhgj">link to 7.1</a></td>
    </tr>
    <tr>
      <td style="text-align: left">8</td>
      <td style="text-align: left"><a href="./docs/pages/symbols.html">A note on symbols, math, and logic</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/zzpsdkpsyrcc">link</a></td>
    </tr>
    <tr>
      <td style="text-align: left">9</td>
      <td style="text-align: left"><a href="./docs/pages/lfg.html">A note on LFG and HPSG</a></td>
      <td style="text-align: left"><a href="https://www.overleaf.com/read/rncbfbxchcvs">link</a></td>
    </tr>
  </tbody>
</table>

<h2 id="notes-on-using-the-website">Notes on using the website:</h2>

<ul>
  <li>You can use the examples of code on the pages of this website as templates (unless noted otherwise); they’ll work fine if you have loaded the necessary packages in the preamble and are using the correct engine.</li>
  <li>Unless noted otherwise, the code given on the pages can be compiled in LaTeX, most of it also in pdfLaTeX.</li>
  <li>The examples are aimed at beginners, and so are not distinct with excessive use of macros for the sake of efficiency and so on. The simplest, even if laborious, way is (usually, but not always) taken. Pages 7-9 tend to have ever so slightly more complex syntax.</li>
  <li>Please experiment as much as you can. The great thing about TeX is that you cannot run something to the effect of <code>sudo rm -R...</code> (a very dangerous thing), so pretty much any experimenting is safe since you can retrace your steps.</li>
  <li>Please make sure to read the blocks with code as there are useful comments on the structure of the code marked by %; see an example below. These comments are important to understand what a given package or command is doing.</li>
</ul>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="c">% Here is a comment you really should read.</span>
and here follows some code which you hopefully understand having read the comment above
</code></pre></div></div>

<hr />

<h2 id="contributions-are-most-welcome-if-you-would-like-to-contribute-or-note-errors-or-something-to-that-effect-please-reach-out-to-dozunorthwesternedu-thanks">Contributions are most welcome! If you would like to contribute, or note errors, or something to that effect, please reach out to <a href="mailto:doz@u.northwestern.edu">doz@u.northwestern.edu</a>. Thanks!</h2>

<hr />

<blockquote>
  <p>Fun fact: TeX is Turing-complete, which both lets you do <a href="https://www.overleaf.com/read/ckbsvjnrfbhc">this</a>, and also makes things ever so slightly undecidable.</p>
</blockquote>

<blockquote>

</blockquote>
:ET