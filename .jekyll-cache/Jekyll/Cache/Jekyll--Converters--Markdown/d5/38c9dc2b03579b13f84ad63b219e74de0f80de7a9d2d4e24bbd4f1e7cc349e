I"�]<h1 id="compiled-examples-are-here-7-and-here-71">Compiled examples are <a href="https://www.overleaf.com/read/vqysvzvqjkxx">here (7)</a> and <a href="https://www.overleaf.com/read/srvqkqcwwhgj">here (7.1)</a>.</h1>

<hr />

<h2 id="arrows-with-sentences">Arrows with sentences</h2>

<p>It is very frequent that those of us working on syntax (predominantly) need to draw lines within a tree; it is not always convenient or desirable to waste space for an entire tree where a linear diagram would do nicely.</p>

<p>This section will proceeds as follows:</p>
<ul>
  <li>first, I’ll show the option of pst-nodes</li>
  <li>then I’ll show example of using topaths TikZ livrary</li>
  <li>then come even more TikZ options</li>
  <li>and then I’ll argue – very subjectively – that all of those above are suboptimal for the purposes of this website (and in some other respects) and suggest my own way.</li>
</ul>

<p>So, I shall not describe the first three in detail, just giving an example of each and citing the source where syntax comes from.</p>

<h3 id="pst-nodes">pst-nodes</h3>

<p>Indeed, in expex documentation, a seeming way go about this is given: by using expex with pstricks. See more information on interaction of these two in documentation of expex (but see also the documentation of pstricks <a href="https://ctan.org/pkg/pstricks-base?lang=en">here</a>). However, pstricks seems to require a separate .sty document and some not insignificant fine-tuning to boot. It seems that it is very difficult to compile pstricks/expex combination successfully without going to quite some trouble locally – let alone on Overleaf.</p>

<p>Therefore, perhaps the most straightforward way here would be to use either (a) pst-node package or (b) topaths library for TikZ. Examples for both are given below. Note also that there <em>definitely</em> are many other ways to achieve the same goal, even in TikZ alone (it’s phenomenally versatile with a wealth of macros and libraries).</p>

<p>Here’s an example which uses pst-nodes, syntax due to <a href="https://tex.stackexchange.com/a/408181/272269">tex.stackexchange.com/a/408181/272269</a>(see the requisite packages there):</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\pex</span>
This is a very long<span class="k">\Rnode</span><span class="p">{</span>st<span class="p">}{</span> <span class="k">\underline</span><span class="p">{</span>sentence that<span class="p">}}</span> appears in this very <span class="k">\Rnode</span><span class="p">{</span>ss<span class="p">}{</span><span class="k">\underline</span><span class="p">{</span>short short<span class="p">}}</span> document.
<span class="k">\ncarc</span><span class="na">[arrows = -&gt;, linecolor =blue, arcangle = 15, nodesep = 1pt]</span><span class="p">{</span>st<span class="p">}{</span>ss<span class="p">}</span>
<span class="k">\ncbar</span><span class="na">[arrows = -&gt;, linecolor =red, angle = -90, arm = 0.75em]</span><span class="p">{</span>ss<span class="p">}{</span>st<span class="p">}</span>
<span class="k">\xe</span>
</code></pre></div></div>

<blockquote>
  <p>Note that this <em>does not</em> compile in pdfLaTeX, in which LSA’s templates are set. This requires XeLaTeX. It does not compile in LuaLaTeX either.</p>
</blockquote>

<h3 id="to-paths">to-paths</h3>

<blockquote>
  <p>The first thing to know is that this clashes with forest package, so you won’t be able to have both forest trees and to-paths diagrams in one document without further trouble.</p>
</blockquote>

<p>Here’s an example which uses to-paths, syntax again due to <a href="https://tex.stackexchange.com/a/408181/272269">tex.stackexchange.com/a/408181/272269</a>(see the requisite packages there):</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\pex</span> This is a  very long <span class="k">\tikz</span><span class="na">[baseline=(node1.base)]</span><span class="k">\node</span> (node1)  <span class="p">{</span><span class="k">\underline</span><span class="p">{</span>sentence that<span class="p">}}</span>; appears
in this very <span class="k">\tikz</span><span class="na">[baseline=(node2.base)]</span><span class="k">\node</span> (node2) <span class="p">{</span><span class="k">\underline</span><span class="p">{</span>short short<span class="p">}}</span>; document.

<span class="nt">\begin{tikzpicture}</span>[overlay]
    <span class="c">% Bend above text line</span>
    <span class="k">\draw</span><span class="na">[-latex]</span> (node2.north) to[bend right] (node1.north);
    <span class="c">% Bend below text line</span>
    <span class="c">%\draw[-latex] (node2.south) to[bend left] (node1.south);</span>
    <span class="c">% Angled</span>
    <span class="k">\draw</span><span class="na">[-latex]</span> (node2.south) -- ++(0,-1.5ex) -| (node1.south);
<span class="nt">\end{tikzpicture}</span>
<span class="k">\xe</span>
</code></pre></div></div>

<h3 id="and-more-tikz">…and more TikZ</h3>

<p>Here’s another option to do this, syntax due to Gonzalo Medina from <a href="https://tex.stackexchange.com/questions/231041/how-to-use-pstricks-with-expex-to-draw-arrows-in-examples?rq=1">tex.stackexchange.com (links to the question)</a>:</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\ex</span>
W<span class="k">\Tikzmark</span><span class="p">{</span>enda<span class="p">}{</span>h<span class="p">}</span><span class="k">\Tikzmark</span><span class="p">{</span>endb<span class="p">}{</span>o<span class="p">}</span>m(A) did John persuade <span class="k">\Tikzmark</span><span class="p">{</span>starta<span class="p">}{</span>t<span class="p">}</span>(B) [ PRO to visit w<span class="k">\Tikzmark</span><span class="p">{</span>startb<span class="p">}{</span>h<span class="p">}</span>om(C) ]
<span class="k">\xe</span>
<span class="k">\DrawArrow</span><span class="p">{</span>starta<span class="p">}{</span>enda<span class="p">}{</span>above<span class="p">}{$</span><span class="nb">M</span><span class="p">_{</span><span class="nb">sp</span><span class="p">}</span><span class="o">=</span><span class="m">2</span><span class="p">$}</span>
<span class="k">\DrawArrow</span><span class="p">{</span>startb<span class="p">}{</span>endb<span class="p">}{</span>above,pos=0.15<span class="p">}{$</span><span class="nb">M</span><span class="p">_{</span><span class="nb">sp</span><span class="p">}</span><span class="o">=</span><span class="m">6</span><span class="p">$}</span>[3.5]
</code></pre></div></div>

<p>All of these examples are compiled following the link to 7 (not 7.1) on the main page, or at the top or at the bottom of this page.</p>

<p>Some other options I encountered upon a brief search are these: <a href="https://tex.stackexchange.com/questions/394271/syntax-movement-arrows-in-examples-in-xelatex-in-beamerposter">here</a>, <a href="https://tex.stackexchange.com/questions/140142/movement-arrow-in-gloss">here</a>, <a href="https://tex.stackexchange.com/questions/225257/linguistics-what-are-the-best-packages-to-use-create-bracketed-structures-with">here</a>, <a href="https://tex.stackexchange.com/questions/380954/long-distance-movement-arrow-in-gloss?rq=1">here</a>, <a href="https://tex.stackexchange.com/questions/353116/movement-arrows-in-gloss-part-iii">here</a>, and <a href="https://tex.stackexchange.com/questions/140142/movement-arrow-in-gloss">here</a>.</p>

<h3 id="why-not-use-the-options-above">Why not use the options above</h3>

<p>I think the options above are suboptimal for various reasons: one is inflexibility in requiring XeLaTeX, another is clashing with forest, yet another would be complexity of some of the syntax. For example, Medina’s example seems simple enough, but at the cost of expanding the preamble with custom settings (see the page linked above for those). These expansions are fine and convenient, but using them without understanding what’s going on (hence without being able to fix it should something go wrong) – as most people (remember the target audience of this website) would use this – is resolutely suboptimal. On the other hand, forest is already familiar to everyone through trees.</p>

<h3 id="forest">Forest!</h3>

<p>I like forest, I think it’s great. So I see no reason not to use forest for the purposes of PRO-type diagrams. It seems there’s no tree, but trees can come in different shapes. This is a tree:</p>

<blockquote>
  <p>Note that examples starting with the one immediately below and until a notice like this below will not be in examples in the compiled document (mainpage link to 7.1).</p>
</blockquote>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code>[ [ [][] ] [ [][] ] ]
</code></pre></div></div>

<p>but this is a tree as well:</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code>[] [] [] [] [] [] []
</code></pre></div></div>

<p>So, I suggest that we use arrows we used with trees (they can be rectangular as well) while splitting the original sentence in parts by putting them in separate nodes, like in the example above. For example, a simple partition would look like this:</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code>[The] [curse] [has] [come] [upon] [me] [cried] [the] [Lady] [of] [Shalott.]
</code></pre></div></div>

<p>It’s not entirely necessary to separate everything. That is, if you want to draw a line from <code>[Lady]</code> to <code>[curse]</code>, a partition like this should do:</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code>[The] [curse] [has come upon me cried the] [Lady] [of Shalott.]
</code></pre></div></div>

<p>Interestingly, examples in the literature often use similar kinds of notation to demarcate boundaries of phrases or similar units, e.g. [ForceP [ [TopP [ [FocP [ [TopP’ [ [FinP [ [IP ]]]]]]]]]]]. So, it might become somewhat difficult to put these units into nodes of a forest-tree. For a simple example, consider [ForceP [TopP [FocP]]]. Also note that to mark that something is a single unit in LaTeX, for whatever purposes, curly braces are often used: cf. <code>\textit{word} word word</code> vs. <code>\textit{word word word}</code>. So,</p>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code>[<span class="p">{</span> [ForceP <span class="p">}</span>] [<span class="p">{</span> [TopP <span class="p">}</span>] [<span class="p">{</span> [FocP <span class="p">}</span>] [<span class="p">{</span> ]]] <span class="p">}</span>]
</code></pre></div></div>

<p>should do. The partitions here are <code>[ForceP</code>, then <code>[TopP</code>, then <code>[FocP</code>, then <code>]]]</code>. Each of the partitions is within <code>{}</code>, indicating it’s a single bit of code; and within <code>[ ]</code>, indicating a forest-node. Now let’s consider some real examples.</p>

<blockquote>
  <p>The examples below are available in the compiled form following the link to 7.1 on the homepage, or the one at the top/bottom of this page.</p>
</blockquote>

<p>Note that apart from forest, the examples below will require <code>\usepackage{fixltx2e}</code> (for <code>\textsubscript{}</code>, etc.) and <code>\usetikzlibrary{matrix}</code>. In terms of fixltx2e, if you can get to superscripts and subscripts some other way – feel free to. I do recommend staying away from math mode if there’s no real math, as it easy to lose track, and LaTeX will throw many <code>Missing $ inserted</code> as well as <code>Missing { inserted</code> at you.</p>

<blockquote>
  <p>This example is from Masaya Yoshida’s “Constraints and Mechanisms in Long-Distance Dependency Formation”, ex. 4 on p. 376. University of Maryland, 2006.</p>
</blockquote>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\pex</span>
<span class="c">% this will be two-part example: one with rectangular arrow, one with elliptical one</span>
<span class="k">\a</span>
<span class="c">% this helps your sentence stay together (try the example without it and see what happens)</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
<span class="c">% this bit is helpful as well; you saw phantom bit in multidominance trees already</span>
<span class="c">% so the structure is not just [tree], but really [,phantom, [tree]]</span>
[,phantom,
<span class="c">% this is the partition itself</span>
[<span class="p">{</span>Wh-NP-Dat-<span class="p">}</span>,name=1] [...] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>NP<span class="p">}</span> GNC<span class="p">}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>NP<span class="p">}}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>CP<span class="p">}}</span>] [<span class="k">\textit</span><span class="p">{</span>Op<span class="p">}</span>] [<span class="p">{</span><span class="k">\textsubscript</span><span class="p">{</span>IP<span class="p">}}</span>] [Subject] [<span class="p">{</span>...<span class="p">}</span>,name=2] [<span class="p">{</span>]]<span class="p">}</span>] [<span class="p">$</span><span class="nb">NP</span><span class="p">_{</span><span class="nb">host</span><span class="p">}$</span>] [<span class="p">{</span>]<span class="p">}</span>] ]
<span class="c">% not to the arrows, just as in an ordinary tree</span>
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="c">% this is syntax for rectangular-shaped arrows</span>
<span class="c">% south is self-explanatory; see notes on other bits below</span>
<span class="k">\draw</span><span class="na">[-&gt;, dashed, &gt;=latex]</span> (1.south) |- ++(0,-0.4) -| (2.south);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="c">% ending the fiest forest, and inserting a vertical space so that the arrow doesn't interfere with the example below</span>
<span class="nt">\end{forest}</span> <span class="k">\vspace</span><span class="p">{</span>1em<span class="p">}</span>
<span class="c">% second example</span>
<span class="k">\a</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
[,phantom,
[<span class="p">{</span>Wh-NP-Dat-<span class="p">}</span>,name=1] [...] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>NP<span class="p">}</span> GNC<span class="p">}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>NP<span class="p">}}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>CP<span class="p">}}</span>] [<span class="k">\textit</span><span class="p">{</span>Op<span class="p">}</span>] [<span class="p">{</span><span class="k">\textsubscript</span><span class="p">{</span>IP<span class="p">}}</span>] [Subject] [<span class="p">{</span>...<span class="p">}</span>,name=2] [<span class="p">{</span>]]<span class="p">}</span>] [<span class="p">$</span><span class="nb">NP</span><span class="p">_{</span><span class="nb">host</span><span class="p">}$</span>] [<span class="p">{</span>]<span class="p">}</span>]
]
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="c">% the usual setting, which we saw on the trees page</span>
<span class="c">% one can adjust looseness as desired to control how far above or below the arrow goes</span>
<span class="k">\draw</span><span class="na">[-&gt;,looseness=0.3,overlay]</span> (1) to[out=south,in=south] (2);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="nt">\end{forest}</span>
<span class="k">\xe</span>
</code></pre></div></div>

<p>Let’s consider the fragment <code>\draw[-&gt;, dashed, &gt;=latex] (1.south) |- ++(0,-0.4) -| (2.south);</code> in some more detail. It’s pretty similar to what we had on the page with trees, but south went into the parentheses with 1 and 2, and the new fragment <code>|- ++(0,-0.4) -|</code> appeared. In this fragment, there are two important parts. Both have to do with <code>-0.4</code>. The sign controls where your arrow is coming from: <code>-</code> is for below, <code>+</code> is for above. Make sure to use either <code>south</code> combined with <code>-</code>, or <code>north</code> combined with <code>+</code> (try <code>+</code> with <code>north</code> and see what happens though). Lastly, the number itself (<code>0.4</code> in the case above) controls how far below or above your arrow goes. Two more examples are below.</p>

<blockquote>
  <p>The following example is Cedric Boeckx and Norbert Horstein’s “Superiority, Reconstruction, and Island” (p. 198, ex. 4) in “Foundational Issues in Linguistic Theory”, Freidin, Otero, and Zubizarreta, eds. MIT Press 2008.</p>
</blockquote>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\pex</span>
<span class="k">\a</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
[,phantom,
[<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>CP<span class="p">}}</span>] [<span class="p">{</span><span class="k">\_</span><span class="p">}</span>,name=1] [<span class="p">{</span>[C<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}}</span>] [<span class="p">{</span><span class="k">\textsuperscript</span><span class="p">{</span>IP<span class="p">}}</span>] [<span class="p">{</span>koj<span class="p">}</span>,name=2] [<span class="p">{</span>[I<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>IP<span class="p">}}</span>] [kakvo] [V<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}</span>] [kogo] [<span class="p">{</span>]]]]]<span class="p">}</span>]
]
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="k">\draw</span><span class="na">[-&gt;,looseness=0.3,overlay]</span> (2) to[out=south,in=south] (1);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="nt">\end{forest}</span> <span class="k">\vspace</span><span class="p">{</span>1em<span class="p">}</span>
<span class="k">\a</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
[,phantom,
[<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>CP<span class="p">}}</span>] [<span class="p">{</span><span class="k">\_</span><span class="p">}</span>,name=1] [<span class="p">{</span>[C<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}}</span>] [<span class="p">{</span><span class="k">\textsuperscript</span><span class="p">{</span>IP<span class="p">}}</span>] [<span class="p">{</span>koj<span class="p">}</span>,name=2] [<span class="p">{</span>[I<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}}</span>] [<span class="p">{</span>[<span class="k">\textsubscript</span><span class="p">{</span>IP<span class="p">}}</span>] [kakvo] [V<span class="k">\textsuperscript</span><span class="p">{</span>0<span class="p">}</span>] [kogo] [<span class="p">{</span>]]]]]<span class="p">}</span>]
]
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="k">\draw</span><span class="na">[-&gt;, dotted, &gt;=latex]</span> (2.south) |- ++(0,-0.3) -| (1.south);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="nt">\end{forest}</span>
<span class="k">\xe</span>
</code></pre></div></div>

<p>In the last one, the partitions are rather complex.</p>

<blockquote>
  <p>The following example (the structure, not the formatting) is from Uli Sauerland’s “Flat Binding”, p.239, ex. (83) in “Interface+Recursion=Language?”, Sauerland and Gartner, eds. Mouton de Gruyter 2007.</p>
</blockquote>

<div class="language-tex highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">\pex</span>
<span class="k">\a</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
[,phantom,
<span class="c">% there's no way out of math mode here; \phi cannot be got at in any other way</span>
<span class="c">% also, in the original example, D subscript was a Greek letter, but html for this website would have none of that</span>
[<span class="p">{</span>the child who <span class="p">$</span><span class="nv">\phi</span><span class="p">$</span>P[the child]<span class="p">$_{</span><span class="nb">D'</span><span class="p">}$</span> dropped the<span class="p">}</span>] [<span class="p">{$</span><span class="o">[-]</span><span class="p">_{</span><span class="nv">\text</span><span class="p">{</span><span class="nb">F</span><span class="p">}}$}</span>,name=1] [<span class="p">{</span>didn't pick up<span class="p">}</span>] [<span class="p">{$</span><span class="nv">\phi\text</span><span class="p">{</span><span class="nb">P</span><span class="p">}</span><span class="o">[</span><span class="p">$</span>the<span class="p">$</span><span class="nb"> </span><span class="o">-]</span><span class="p">_</span><span class="nb">D</span><span class="p">$}</span>,name=2]
]
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="k">\draw</span><span class="na">[-,looseness=0.3,overlay]</span> (2) to[out=south,in=south] (1);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="nt">\end{forest}</span> <span class="k">\vspace</span><span class="p">{</span>1em<span class="p">}</span>
<span class="k">\a</span>
<span class="k">\pgfkeys</span><span class="p">{</span>/pgf/inner sep=0.05em<span class="p">}</span>
<span class="nt">\begin{forest}</span>
[,phantom,
[<span class="p">{</span>the child who <span class="p">$</span><span class="nv">\phi</span><span class="p">$</span>P[the child]<span class="p">$_{</span><span class="nb">D'</span><span class="p">}$</span> dropped the<span class="p">}</span>] [<span class="p">{$</span><span class="o">[-]</span><span class="p">_{</span><span class="nv">\text</span><span class="p">{</span><span class="nb">F</span><span class="p">}}$}</span>,name=1] [<span class="p">{</span>didn't pick up<span class="p">}</span>] [<span class="p">{$</span><span class="nv">\phi\text</span><span class="p">{</span><span class="nb">P</span><span class="p">}</span><span class="o">[</span><span class="p">$</span>the<span class="p">$</span><span class="nb"> </span><span class="o">-]</span><span class="p">_</span><span class="nb">D</span><span class="p">$}</span>,name=2]
]
<span class="nt">\begin{pgfinterruptboundingbox}</span>
<span class="k">\draw</span><span class="na">[-, &gt;=latex]</span> (2.south) |- ++(0,-0.3) -| (1.south);
<span class="nt">\end{pgfinterruptboundingbox}</span>
<span class="nt">\end{forest}</span>
<span class="k">\xe</span>
</code></pre></div></div>

<p>Please take a look at the compiled examples. Ultimately, there’s no difference in terms of how the goal is achieved – whether with forest, topaths, or something else – it’s only a matter of convenience and getting the right output.</p>

<hr />

<h1 id="compiled-examples-are-here-7-and-here-71-1">Compiled examples are <a href="https://www.overleaf.com/read/vqysvzvqjkxx">here (7)</a> and <a href="https://www.overleaf.com/read/srvqkqcwwhgj">here (7.1)</a>.</h1>

<p><a href="https://dozernyi.gitlab.io/lsalatex">back</a></p>
:ET