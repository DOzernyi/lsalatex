---
layout: default
---
## About

This website should provide a more or less comprehensive guide to LaTeX which is used in the LSA templates, namely:

* <em>Proceedings of the Linguistic Society of America</em> (template by Sarah E. Murray, Patrick Farrell, Daniil M. Ozernyi)
* <em>Phonological Data and Analysis </em> (template by Daniil M. Ozernyi, Patrick Farrell, Laura R. Faircloth)
* we also link the template for <em>Experiments in Linguistic Meaning</em> (template by Florian Schwarz), but please see <em>ELM</em>'s [website](https://journals.linguisticsociety.org/proceedings/index.php/ELM/pages/view/instructions) for instructions.

While the templates do contain examples of bare essentials, they do not introduce readers to TeX and are primarily aimed at those already familiar with it. So, this website will hopefully cover the basics of most packages needed for classroom use (be it a syntax, semantics, phonology, or some other class requiring TeX) or for LSA templates. Note that TikZ is not discussed at length (only in conjunction with expex), and neither is BibLaTeX. The stress is on [Overleaf (links to an external site)](https://www.overleaf.com/project), which is (pretty much without doubt) easier for beginners both to grasp and to use.

My hope is that the information given here and the ease of accessing [Overleaf](https://www.overleaf.com/project) will encourage more people to use (La)TeX in their work.

**Crucially**, however, the usage of the templates given here is **by no means** restrictive: provided that the final submission fits the requirements given by PLSA/PDA/ELM, any means to get that result is perfectly fine.

> Before you look at the pages below and the examples of implementation of the content covered here, please do consult a (very) concise Overleaf 30-minute introduction to LaTeX [here](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes).

|   | Page with brief intro and code with comments          | Overleaf link with clean code and compiled examples    |
|:--|:---------------------------------------------------------------|:------------------|
| 1 | [Compiling (Overleaf, local)](./docs/pages/compiling.html)                  | -- |
| 2 | [Trees (qtree, forest, multidominance, lambda)](./docs/pages/trees.html)            | [link](https://www.overleaf.com/read/pwvbtzfknmcj)   |
| 3 | [Glosses and examples (expex)](./docs/pages/gloss.html)                     | [link](https://www.overleaf.com/read/pncdfwhgdqzx)      |
| 4 | [IPA (tipa, some notes on XeLaTeX/LuaLaTeX)](./docs/pages/ipa.html)         | -- |
| 5 | References (natbib, .bst) [TBD]                                            | [TBD] |
| 6 | [Tableaux (just tables, really; tabularray and tabular)](./docs/pages/tabl.html) | [link](https://www.overleaf.com/read/nsvwmfnjzsyg) |
| 7 | [PRO- and trace-arrows for linear examples](./docs/pages/pro.html)          | [link to 7](https://www.overleaf.com/read/vqysvzvqjkxx), [link to 7.1](https://www.overleaf.com/read/srvqkqcwwhgj) |
| 8 | [A note on symbols, math, and logic](./docs/pages/symbols.html)             | [link](https://www.overleaf.com/read/zzpsdkpsyrcc) |
| 9 | [A note on LFG and HPSG](./docs/pages/lfg.html)                             | [link](https://www.overleaf.com/read/rncbfbxchcvs) |


## Notes on using the website:

* You can use the examples of code on the pages of this website as templates (unless noted otherwise); they'll work fine if you have loaded the necessary packages in the preamble and are using the correct engine.
* Unless noted otherwise, the code given on the pages can be compiled in LaTeX, most of it also in pdfLaTeX.
* The examples are aimed at beginners, and so are not distinct with excessive use of macros for the sake of efficiency and so on. The simplest, even if laborious, way is (usually, but not always) taken. Pages 7-9 tend to have ever so slightly more complex syntax.
* Please experiment as much as you can. The great thing about TeX is that you cannot run something to the effect of <code>sudo rm -R...</code> (a very dangerous thing), so pretty much any experimenting is safe since you can retrace your steps.
* Please make sure to read the blocks with code as there are useful comments on the structure of the code marked by %; see an example below. These comments are important to understand what a given package or command is doing.  

```tex
% Here is a comment you really should read.
and here follows some code which you hopefully understand having read the comment above
```

* * *

## Contributions are most welcome! If you would like to contribute, or note errors, or something to that effect, please reach out to <a href="mailto:doz@u.northwestern.edu">doz@u.northwestern.edu</a>. Thanks!

* * *

> Fun fact: TeX is Turing-complete, which both lets you do [this](https://www.overleaf.com/read/ckbsvjnrfbhc), and also makes things ever so slightly undecidable.

>
